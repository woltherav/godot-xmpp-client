###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###
extends Control

onready var btn_add_account = get_node("TabContainer/Accounts/AccountLayout/AccountContainer/BtnAddAccount")
onready var dlg_add_account = get_node("DlgAddAccount")
onready var dlg_error = get_node("DlgError")
onready var account_list = get_node("TabContainer/Accounts/AccountLayout/AccountContainer/AccountScroll/AccountList")
onready var debug_output = get_node("TabContainer/Debug/Panel/DebugOutput")
onready var message_handler = get_node("TabContainer/Chat/MessageHandler")

const Account = preload("res://xmpp_client/control_nodes/account.tscn")

###
# Main control that holds the client.
###

func _ready():

	btn_add_account.connect("pressed", dlg_add_account, "popup_centered")
	dlg_add_account.connect("confirmed", self, "add_account")
	debug_output.clear()
	pass


func add_account():
	var port_number = dlg_add_account.get_port_number()
	var server_ip = dlg_add_account.get_server_address()
	var account_name = dlg_add_account.get_account_name()
	var password = dlg_add_account.get_password()

	debug_to_output(str("Port: % Server: % Account: %").format([port_number, server_ip, account_name], "%"))

	var acc = Account.instance()
	account_list.add_child(acc)
	acc.setup_stream(server_ip, port_number, account_name, password)
	acc.connect("debug", self, "debug_to_output")
	acc.connect("error_message", self, "receive_error_message")
	acc.connect("stream_failed", self, "remove_account")
	acc.get_signal_list()

	message_handler.active_chat_widget.add_account(acc)

func remove_account(account_name:String):
	for account in account_list.get_children():
		if account.account_name == account_name:
			message_handler.active_chat_widget.remove_account(account)
			account.disconnect("debug", self, "debug_to_output")
			account.disconnect("error_message", self, "receive_error_message")
			account.disconnect("stream_failed", self, "remove_account")
			account_list.remove_child(account)
			account.queue_free()

func debug_to_output(debug_data:String):
	#print(debug_data)
	debug_output.append_bbcode("\n"+debug_data)

func receive_error_message(error_message:String, full_error:String, _account_name:String):
	dlg_error.set_error(error_message, full_error)
	dlg_error.show()
