###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends VBoxContainer

###
# Element to show a question during chat. Currently used for subscriptions.
###

onready var subject_label = get_node("Header/Subject")
onready var rich_text = get_node("RichTextLabel")
onready var button_layout = get_node("ButtonLayout")

func _ready():
	pass

###
# Add a button.
###
func add_button(button:Button):
	button_layout.add_child(button)

###
# Hide and remove all buttons.
###
func hide_and_remove_buttons():
	hide()
	for button in button_layout.get_children():
		button_layout.remove_child(button)
