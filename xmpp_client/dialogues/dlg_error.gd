###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends AcceptDialog

###
# Dialog to show error messages
###

onready var error_xml = get_node("VBoxContainer/Panel/RichTextLabel")

func set_error(error_message:String, error_stanza:String):
	self.dialog_text = error_message
	self.error_xml.bbcode_text = error_stanza

