###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends AcceptDialog

###
# Dialog to add an account.
###

onready var port_input = get_node("GridContainer/lnPort")
onready var server_input = get_node("GridContainer/lnServer")
onready var account_input = get_node("GridContainer/lnAccount")
onready var password_input = get_node("GridContainer/lnPassword")

func _ready():

	pass

func get_port_number():
	return port_input.text.to_int()

func get_server_address():
	return server_input.text

func get_account_name():
	return account_input.text

func get_password():
	return password_input.text
