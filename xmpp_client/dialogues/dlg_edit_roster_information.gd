###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends ConfirmationDialog

###
# Dialog for changing roster item information.
###
onready var friendly_name_input = get_node("main_layout/lnFriendlyName")
onready var jid_entry = get_node("main_layout/JIDEntry")
onready var groups_input = get_node("main_layout/lnGroups")

export(String) var friendly_name = "" setget set_friendly_name, get_friendly_name
export(String) var jid = "" setget set_JID
export(Array) var groups = [] setget set_groups, get_groups

func _ready():
	pass

func set_friendly_name(val:String):
	friendly_name_input.text = val

func get_friendly_name() -> String:
	return friendly_name_input.text

func set_JID(val:String):
	jid_entry.text = val

func set_groups(val:Array):
	groups_input.text = PoolStringArray(val).join(", ")

func get_groups() -> Array:
	var raw = groups_input.text.split(",")
	var polished = []
	for group in raw:
		polished.append(group.strip_edges())
	return polished
