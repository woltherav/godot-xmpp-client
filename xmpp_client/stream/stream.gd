###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends ProgressBar

####
# A class for handling an XMPP stream. Implemented as a progress bar so you
# can see the current state.
#
# XMPP streams are tcp streams, that start of with a number of negotiations.
# This class first goes through the server connection, ssl negotiation and
# the SASL authentication. After that it sits and listens to the connection,
# waiting for incoming stanzas.
#
# When complete stanzas have arrived, you can receive them with the 'new_stanza'
# signal. You can properly close the connection with end_stream()
# The signal debug will send up debug information.
# The signal error will send up error messages.
#
# TODO:
# - Connection maintainance, such as pinging when there's no activity.
# - Better consideration of streampeer failing to connect.
####

var tcp_peer = StreamPeerTCP.new()

var server_ip = "127.0.0.1" setget set_server_ip, get_server_ip
var account_name = "testuser1@localhost" setget set_account_name, get_account_name
var port_number = 5222  setget set_port_number, get_port_number
var password = "test" setget set_password, get_password
var locale = "en"

signal debug(debug_data)
signal new_stanza(stanza)
signal error_message(error_message, error_stanza)

var count_connecting_time = 0
var partial_stanza := ""

enum StreamState {
	END, # Stream is over. Starting with this.
	START, # Stream hass started.
	TLS, # Connection has been made and the first header is send. Let's get ssl!
	AUTHENTICATE, # We have negotiated whether to use TLS, let's authenticate.
	STANZA # We have authenticated, account is now allowed to send stanzas
}
var stream_status = StreamState.END

func _init():
	self.min_value = StreamState.END
	self.max_value = StreamState.STANZA

func _process(delta):
	self.value = stream_status

	if (tcp_peer.get_status() == StreamPeerTCP.STATUS_CONNECTED) :
		if (tcp_peer.has_method("poll")):
			tcp_peer.poll()
		if  tcp_peer.get_available_bytes()>0:
			var response = tcp_peer.get_string(tcp_peer.get_available_bytes())
			emit_signal("debug","Stream: response \n[color=red]"+response+"[/color]")

			if stream_status == self.StreamState.STANZA:
				collect_stanza(remove_stream_header(response))
			else:
				stream_process(remove_stream_header(response))

	if tcp_peer.get_status() == StreamPeerTCP.STATUS_CONNECTING:

		count_connecting_time += delta
	if count_connecting_time>1: # if it took more than 1s to connect, error

		emit_signal("debug","Stream: Stuck connecting, will now disconnect")
		tcp_peer.disconnect() #interrupts connection to nothing
		set_process(false) # stop listening for packets

###
# Start stream connection process.
###
func connect_stream():
	if (tcp_peer.get_status() == StreamPeerTCP.STATUS_CONNECTED):
		end_stream()
	tcp_peer =  StreamPeerTCP.new()
	stream_status = StreamState.START
	stream_process()

###
# Connect to the server ip and port, and start checking whether there's stream
# info yet.
###
func connect_to_server(var server:String, var port:int):
	if tcp_peer.get_status() == StreamPeerTCP.STATUS_CONNECTED:
		pass

	tcp_peer.connect_to_host(server, port)
	count_connecting_time = 0
	set_process(true)

###
# Send a string in the appropriate encoding.
###
func send_string(var stanza:String):
	emit_signal("debug", "Sending data: [color=blue]%[/color]".format([stanza], "%"))
	tcp_peer.put_data(str(stanza).to_utf8())

###
# End the stream.
###
func end_stream():
	emit_signal("debug","Stream: Ending stream")
	send_string("</stream>")
	if tcp_peer.has_method("disconnect_from_stream"):
		tcp_peer.disconnect_from_stream()
	else:
		tcp_peer.disconnect_from_host()
	set_process(false)
	stream_status = StreamState.END

####
# stream_process
#
# Stream process is the most important function, as the negotiation of a stream
# consists of sending headers back and forth.
####
func stream_process(response :String = ""):

	if (!response.empty() and stream_status != self.StreamState.STANZA):
		if response.begins_with("<stream:error"):
			var stream_error = XMPPStreamError.new()
			stream_error.parse_from_xml(response)
			var error_name = stream_error.error_name_for_enum(stream_error.error_type)
			var error = tr('A stream error of type "%" occured, or in other words: % \n\nStream errors cannot be recovered from, the connection will be closed.'.format([error_name, stream_error.human_friendly_error_message()], "%"))
			emit_signal("error_message", error, response)

		elif response.begins_with("<stream:features"):
			var stream_features = XMPPStreamFeatures.new()
			stream_features.parse_from_xml(response)

			if stream_features.parsedDictionary.has("starttls"):
				emit_signal("debug","Stream: sending request for ssl")
				var request_tls = "<starttls xmlns='urn:ietf:params:xml:ns:xmpp-tls'/>"
				send_string(request_tls)
				stream_status = self.StreamState.TLS

			elif stream_features.parsedDictionary.has("mechanisms"):
				var authentication_methods = stream_features.parsedDictionary["mechanisms"]
				if (!authentication_methods.empty()):
					emit_signal("debug","Stream: authentication methods: "+PoolStringArray(authentication_methods).join(", "))
					negotiate_sasl(authentication_methods)
					stream_status = self.StreamState.AUTHENTICATE

		else:
			if stream_status == StreamState.TLS:
				if response.begins_with("<proceed"):
					emit_signal("debug","Stream: gotten go ahead for ssl")
					negotiate_tls()
				else:
					emit_signal("error_message", tr("Tls negotiation failed."), response)

			elif stream_status == StreamState.AUTHENTICATE:
				if response.begins_with("<success"):
						stream_status = self.StreamState.STANZA
						start_stream()
						return
				else:
					var parser = XMLParser.new()
					parser.open_buffer(response.to_utf8())
					var failure_text = []
					while parser.read() == OK:
						if parser.get_node_type() == XMLParser.NODE_TEXT:
							failure_text.append(parser.get_node_data())
					print(failure_text)
					failure_text = tr("Authentication failed! %".format([PoolStringArray(failure_text).join(" ")], "%"))
					emit_signal("error_message", failure_text, response)

	elif stream_status == self.StreamState.START:
		connect_to_server(server_ip, port_number)
		start_stream()
	else:
		print("Mystery stream status: "+str(stream_status))

####
# Send the stream header.
# Needs to be done several times over stream negotiation.
####
func start_stream():
	var server_name = account_name.split("@")[-1]

	var streamstart = []
	var xmlVersion= "<?xml version='1.0'?>\n"
	streamstart.append("<stream:stream")
	streamstart.append("xmlns='jabber:client'")
	streamstart.append("xmlns:stream='http://etherx.jabber.org/streams'")
	streamstart.append("from='%'".format([account_name], "%"))
	streamstart.append("to='%'".format([server_name], "%"))
	streamstart.append("version='1.0'")
	streamstart.append("xml:lang='%'".format([locale], "%"))
	streamstart.append(">")

	streamstart = PoolStringArray(streamstart).join(" ")

	send_string(str(xmlVersion + streamstart))

###
# Try to switch to an SSL based connection.
###
func negotiate_tls():

	var ssl_peer = StreamPeerSSL.new()
	# I am unsure how to validate the ssl certificate for an unknown host?
	var ssl_succes = ssl_peer.connect_to_stream(tcp_peer)
	print(ssl_succes)
	if ssl_succes == OK:
		emit_signal("debug","Stream: switched to SSL!")
		tcp_peer = ssl_peer
		start_stream()
	else:
		emit_signal("error_message", "SSL failed, error %".format([ssl_succes], "%"), str(ssl_peer.get_status()))

###
# Try to authenticate using SASL. We can currently only do plain.
# For SCRAM based methods, we need HMAC at the very least.
###
func negotiate_sasl(authentication_methods : Array):
	if (!authentication_methods.has("PLAIN")):
		end_stream()
	emit_signal("debug","Stream: sending request for plain")

	var auth_account = "\u0000"+account_name.split("@")[0]
	auth_account += "\u0000"
	auth_account += password
	auth_account = Marshalls.utf8_to_base64(auth_account)

	var request_sasl = []
	request_sasl.append("<auth")
	request_sasl.append("xmlns='urn:ietf:params:xml:ns:xmpp-sasl'")
	request_sasl.append("mechanism='PLAIN'")
	request_sasl.append(">%</auth>".format([auth_account], "%"))

	send_string(PoolStringArray(request_sasl).join(" "))

####
# Remove stream header
# The stream header is send several times during connection/negotiation.
# It is not very useful to recognize whether a stanza is complete, and thus
# needs removal.
#
# Returns text without stream header.
####

func remove_stream_header(text :String) -> String:
	var index = 0
	if text.begins_with("<?") :
		# Strip xml header
		index = text.find("?>")
		text = text.substr(index+2).strip_edges()
	# strip stream header
	var rg = RegEx.new()
	rg.compile("<\\s?(stream|stream:stream)\\s")
	var result = rg.search(text)
	if result:
		emit_signal("debug", "Stream: Response header received")
		index = text.find(">", result.get_end())
		text = text.substr(index+1)
	return text

###
# Due to the nature of the tcp stream, stanzas are expected to be possible
# to be partially sent. We first need to ensure a stanza is complete before we
# send it up the chain to be parsed.
#
# Emits new_stanza when done.
###
func collect_stanza(text : String):

	partial_stanza += text
	if partial_stanza.empty():
		return

	var complete_stanza = true

	while complete_stanza == true:

		var parser = XMLParser.new()
		if partial_stanza.to_utf8().size() == 0:
			return
		parser.open_buffer(partial_stanza.to_utf8())

		var element_name = ""
		var node_offset = 0

		if parser.read() == OK and parser.get_node_type() == XMLParser.NODE_ELEMENT:
			element_name = parser.get_node_name()
		if parser.is_empty():
			node_offset = partial_stanza.to_utf8().size()
			if parser.read() == OK:
				node_offset = parser.get_node_offset()

		while parser.read() == OK and node_offset == 0:
			if parser.get_node_type() == XMLParser.NODE_ELEMENT_END:
				if parser.get_node_name() == element_name:
					if parser.read() == OK:
						node_offset = parser.get_node_offset()
					else:
						node_offset = partial_stanza.to_utf8().size()

		if node_offset > 0:
			# We have found a complete stanza.
			var new_stanza = partial_stanza.to_utf8().subarray(0, node_offset-1).get_string_from_utf8()
			if node_offset == partial_stanza.to_utf8().size():
				partial_stanza = ""
			else:
				partial_stanza = partial_stanza.to_utf8().subarray(node_offset, -1).get_string_from_utf8()
			emit_signal("new_stanza", new_stanza)
		else:
			complete_stanza = false

##################
# Set/Getters -- Are these even necessary given I don't do anything fancy here?
##################

func set_server_ip(value: String):
	server_ip = value

func get_server_ip() -> String:
	return server_ip

func set_account_name(value: String):
	account_name = value

func get_account_name() -> String:
	return account_name

func set_port_number(value: int):
	port_number = value

func get_port_number() -> int:
	return port_number

func set_password(value: String):
	password = value

func get_password() -> String:
	return password
