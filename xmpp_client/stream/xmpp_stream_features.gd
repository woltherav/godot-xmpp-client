###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends Resource

class_name XMPPStreamFeatures

###
# Parser for stream features.
##

export (String) var xml_string = "" setget parse_from_xml
export (Dictionary) var parsedDictionary = {}

enum FeatureReq{
	NO_FEATURE,
	NO_REQ,
	OPTIONAL,
	REQUIRED
}

###
# Remove the entry as it's done.
###
func feature_done(feature:String):
	parsedDictionary.erase(feature)

func parse_from_xml(xml:String):

	var parser = XMLParser.new()

	parser.open_buffer(xml.to_utf8())
	parser.read()

	parse_children(parser, parser.get_node_name())

func parse_children(parser:XMLParser, _parent_name:String):
	parsedDictionary = {}
	while (parser.read() == OK):

		if (parser.get_node_type()== XMLParser.NODE_ELEMENT):
			var node_name = parser.get_node_name()
			if !parser.is_empty():

				match(node_name):
					"mechanisms":
						parsedDictionary[node_name] = parse_option_list(parser, node_name, "mechanism")
					"compression":
						parsedDictionary[node_name] = parse_option_list(parser, node_name, "method")
					_:
						parsedDictionary[node_name] = parse_requirement(parser, node_name)
			else:
				parsedDictionary[node_name] = FeatureReq.NO_REQ
		# else:
		# 	print("Unhandled xml node type: "+str(parser.get_node_type()))

func parse_requirement(parser:XMLParser, parent_name:String):
	var required = self.FeatureReq.NO_REQ
	while (parser.read() == OK):

		if (parser.get_node_type()== XMLParser.NODE_ELEMENT):
			var node_name = parser.get_node_name()
			if parser.is_empty():
				if node_name == "required":
					return self.FeatureReq.REQUIRED
				elif node_name == "optional":
					return self.FeatureReq.OPTIONAL

		elif(parser.get_node_type()== XMLParser.NODE_ELEMENT_END):
			var node_name = parser.get_node_name()
			if node_name == parent_name:
				return required

func parse_option_list(parser:XMLParser, parent_name:String, option_name:String):
	var options = []

	while (parser.read() == OK):
		var _result
		if (parser.get_node_type()== XMLParser.NODE_ELEMENT):
			if parser.get_node_name() == option_name:
				_result =parser.read()
				if (parser.get_node_type()== XMLParser.NODE_TEXT):
					options.append(parser.get_node_data())
				_result = parser.read()
		elif(parser.get_node_type()== XMLParser.NODE_ELEMENT_END):
			if parser.get_node_name() == parent_name:
				return options
