###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends Resource

class_name XMPPStreamError

###
# This is a parser for stream errors, that keeps a list of human friendly error-explainations.
# Stream errors, unlike stanza errors, will always close the stream.
###

enum StreamErrorTypes{
	bad_format,
	bad_namespace_prefix,
	conflict,
	connection_timeout,
	host_gone,
	host_unknown,
	improper_addressing,
	internal_server_error,
	invalid_from,
	invalid_namespace,
	invalid_xml,
	not_authorized,
	not_well_formed,
	policy_violation,
	remote_connection_failed,
	reset,
	resource_constraint,
	restricted_xml,
	see_other_host,
	system_shutdown,
	undefined_condition,
	unsupported_encoding,
	unsupported_feature,
	unsupported_stanza_type,
	unsupported_version
}

var humane_error_messages = {
	StreamErrorTypes.bad_format:
		tr("The sent message cannot be processed at all."),
	StreamErrorTypes.bad_namespace_prefix:
		tr("The namespace cannot be recognized"),
	StreamErrorTypes.conflict:
		tr("This connection conflicts with another connection coming from the same address, which makes it impossible to differentiate between the connections."),
	StreamErrorTypes.connection_timeout:
		tr("This client took too long to respond, and thus the server assumed the device had lost internet connection."),
	StreamErrorTypes.host_gone:
		tr("The address of the server has changed."),
	StreamErrorTypes.host_unknown:
		tr("The server does not know the address."),
	StreamErrorTypes.improper_addressing:
		tr("The address is missing from the sent message."),
	StreamErrorTypes.internal_server_error:
		tr("The server is experiencing issues, try again later."),
	StreamErrorTypes.invalid_from:
		tr("This connection is not allowed to sign it's messages with the given address."),
	StreamErrorTypes.invalid_namespace:
		tr("The message was formatted with an incorrect namespace."),
	StreamErrorTypes.invalid_xml:
		tr("The message was formatted with invalid xml."),
	StreamErrorTypes.not_authorized:
		tr("The client is not allowed to sent these messages because it is not authorized to do so."),
	StreamErrorTypes.not_well_formed:
		tr("The message was formatted with incorrectly formed xml."),
	StreamErrorTypes.policy_violation:
		tr("The server determined the message violated server policy in some technical manner."),
	StreamErrorTypes.remote_connection_failed:
		tr("The server is unable to deliver the message because it cannot connect to the other server."),
	StreamErrorTypes.reset:
		tr("The server closed the connection because it deemed to connection needed to be reset for either feature or security purposes."),
	StreamErrorTypes.resource_constraint:
		tr("The server is too busy to handle this stream."),
	StreamErrorTypes.restricted_xml:
		tr("The message contained restricted xml (such as comments, processing instructions, dtd subset, or an xml entity reference)"),
	StreamErrorTypes.see_other_host:
		tr("The server is redirecting the client to a different host."),
	StreamErrorTypes.system_shutdown:
		tr("The server is being shut down."),
	StreamErrorTypes.undefined_condition:
		tr("A special case error has occured. See included xml stream."),
	StreamErrorTypes.unsupported_encoding:
		tr("The message was encoded with an encoding other than UTF-8, or contained extra bytes that the server is not expecting (as possible with godot's tls functions)."),
	StreamErrorTypes.unsupported_feature:
		tr("The required stream features are not supported."),
	StreamErrorTypes.unsupported_stanza_type:
		tr("The sent message is of a stanza type that the server does not recognize."),
	StreamErrorTypes.unsupported_version:
		tr("This server does not support XMPP version 1.0")
}

var error_type = StreamErrorTypes.undefined_condition
var extra_data = ""

func parse_from_xml(xml:String):
	var parser = XMLParser.new()
	parser.open_buffer(xml.to_utf8())
	parse_with_parser(parser)

func parse_with_parser(parser:XMLParser):

	# Read the error element
	if parser.read() == OK and parser.get_node_name() == "stream:error":
		if parser.is_empty():
			error_type = 0

	# Read the error inside...
	if parser.read() == OK:
		var error_name = parser.get_node_name().replace("-", "_")
		error_type = StreamErrorTypes[error_name]
		if !parser.is_empty() and parser.read() == OK:
			extra_data = parser.get_node_data()
	return OK

func stanza() -> String:
	var stanza = []

	stanza.append("<stream:error>")

	var error_name = error_name_for_enum(error_type)

	if !error_type.empty():
		stanza.append("<"+error_name)
		stanza.append("xmlns='urn:ietf:params:xml:ns:xmpp-streams'")
		if extra_data.empty():
			stanza.append("/>")
		else:
			stanza.append(">"+extra_data+"</"+error_name+">")
	stanza.append("</stream:error>")

	return PoolStringArray(stanza).join(" ")

func error_name_for_enum(value :int):
	var error_name = ""
	for i in StreamErrorTypes.keys():
		if StreamErrorTypes[i] == value:
			error_name = str(i).replace("_", "-")
	return error_name
###
# Human friendly error messages for the error dialogue.
###
func human_friendly_error_message():
	return humane_error_messages[error_type]
