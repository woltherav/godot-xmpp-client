###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends Tree

const Account = preload("res://xmpp_client/control_nodes/account.gd")
const Roster = preload("res://xmpp_client/xmpp/Roster/xmpp_roster.gd")


###
# This handles the layout of the roster ui. Each acount gets a tree item, and
# each roster item gets a sub tree item under that. Clicking a roster tree-item
# will result
#
# TODO:
# - Have the chatview also be notified? Useful for handling subscriptions better.
# - Allow users to message a random person?
###

var accounts = []

var tree_root = self.create_item()
# using this for drawing functions.
var font = self.get_font("font")

onready var roster_options = get_node("RosterOptions")

signal roster_info_requested(roster_item)

func _ready():
	var _val = connect("item_rmb_selected", self, "show_roster_options")
	roster_options.connect("id_pressed", self, "handle_roster_item_action")
###
# Change the accounts.
###
func set_accounts(new_accounts:Array):
	accounts = new_accounts
	refill_widget()

###
# Remove all tree items and create a new set.
###
func refill_widget():
	var item = tree_root.get_children()
	while item:
		item.free()
		item = tree_root.get_children()
	for account in accounts:
		add_account(account)

###
# Add a new account, called by the main window.
###
func add_account(account:Account):
	var _val = account.connect("roster_update", self, "handle_account_roster_update")
	_val = account.connect("roster_presence_update", self, "update")
	var account_item = self.create_item(tree_root)
	account_item.set_text(0, account.account_name)
	account_item.set_metadata(0, account)
	account_item.set_selectable(0, false)
	refill_account_roster(account_item)

###
# Remove an account, currently not used.
###
func remove_account(account:Account):
	account.disconnect("roster_update", self, "handle_account_roster_update")
	account.disconnect("roster_presence_update", self, "update")
	get_account_item_by_jid(account.account_name).free()

###
# Returns the tree_item associated with an account for refilling.
###
func get_account_item_by_jid(jid:String) -> TreeItem:
	var account_item = tree_root.get_children()
	while account_item:
		var account = account_item.get_metadata(0)
		if account.account_name == jid:
			return account_item
		account_item = tree_root.get_children()
	return self.create_item(tree_root)

###
# Function to only refill the roster of one account.
###
func refill_account_roster(account_item:TreeItem):
	var item = account_item.get_children()
	while item:
		item.free()
		item = account_item.get_children()
	var account = account_item.get_metadata(0)
	for roster_jid in account.roster.roster_items.keys():
			var roster_item = account.roster.get_roster_item(roster_jid)
			create_roster_entry(account_item, roster_item)

###
# Function only created to simplify the other functions.
###
func create_roster_entry(parent:TreeItem, roster_item:XMPPRosterItem):
	var roster_entry = self.create_item(parent)
	roster_entry.set_text(0, roster_item.jid)
	roster_entry.set_metadata(0, roster_item)
	roster_entry.custom_minimum_height = font.height*2
	roster_entry.set_custom_draw(0, self, "draw_roster_entry")
	roster_entry.set_cell_mode(0, TreeItem.CELL_MODE_CUSTOM)

###
# Drawing the roster entries customly, so that we can show all the information
# inside the roster items in a nice and pleasant way.
###
func draw_roster_entry(item:TreeItem, rect:Rect2):
	var roster_item = item.get_metadata(0)
	if roster_item:

		# By using the selection stylebox we can make sure that these margins
		# still make sense when changing the themes.
		var selection = self.get_stylebox("selected")

		var h_offset = selection.margin_left
		rect.position = rect.position + Vector2(selection.margin_top, selection.margin_left)
		rect.size[0] -= (selection.margin_left + selection.margin_right)
		rect.size[1] -= (selection.margin_top + selection.margin_bottom)
		var avatar_rect = rect
		avatar_rect.size = Vector2(avatar_rect.size[1], avatar_rect.size[1])

		var transparent_color = Color(1, 1, 1, 1)
		if roster_item.presence.type == XMPPPresence.Type.UNAVAILABLE:
			transparent_color.a = 0.5
		if roster_item.presence.show == XMPPPresence.ShowPresence.UNDEFINED:
			transparent_color.a = 0.5
		draw_texture_rect(roster_item.avatar, avatar_rect, false, transparent_color)
		var text = roster_item.friendly_name
		if text.empty():
			text = roster_item.jid

		var text_color = self.get_color("font_color")

		var text_pos = Vector2(avatar_rect.position[0]+avatar_rect.size[1]+h_offset, avatar_rect.position[1]+font.ascent)
		draw_string(font, text_pos, text, text_color)

		var texture = roster_item.presence.get_status_icon()
		var status_rect = Rect2(text_pos+Vector2(0, font.get_descent()), Vector2(font.height,font.height))
		draw_texture_rect(texture, status_rect, false, text_color)

		text_pos = Vector2(text_pos[0]+font.height+h_offset, text_pos[1]+font.height)
		var status = roster_item.presence.status
		if status.empty():
			status = tr("no status")
			text_color.a = 0.5
		var status_width = rect.size[0] - avatar_rect.size[0] - status_rect.size[0] - h_offset
		var status_size = font.get_wordwrap_string_size(status, status_width)
		draw_string(font, text_pos, status, text_color, status_size[0])
	return

func handle_account_roster_update(jid):
	refill_account_roster(get_account_item_by_jid(jid))

func show_roster_options(position:Vector2):
	var bounds = Rect2(position, roster_options.rect_size)
	roster_options.popup(bounds)

func handle_roster_item_action(value:int):
	var roster_item = get_selected().get_metadata(0)

	if !roster_item:
		pass
	# Starting at value 1 here, as one of the entries is 'delete', so we'd
	# want to ensure any odd values result in the information window showing up.
	if value == 1:
		# Send subscription request.
		roster_item.send_subscription_request("")
	elif value == 2:
		# Send unsubscription.
		roster_item.send_unsubscribe()
	elif value == 3:
		# Delete!
		roster_item.subscription_type = XMPPRosterItem.SubscriptionType.REMOVE
		roster_item.update_and_sent_query()
	else:
		emit_signal("roster_info_requested", roster_item)
