###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends HSplitContainer

###
# Message Handler is a bit badly named, used to hold the messages, now mostly
# handles which chats are visible.
###

onready var active_chat_widget = get_node("ActiveChatList")
onready var chat_view = get_node("ChatView")
onready var edit_roster_info = get_node("DlgEditRosterInformation")


func _ready():
	active_chat_widget.connect("item_selected", self, "change_chat")
	active_chat_widget.connect("roster_info_requested", self, "start_edit_roster_info")
	pass

func change_chat():
	var metadata = active_chat_widget.get_selected().get_metadata(0)
	if metadata.get_class() == "Resource":
		var account_name = ""
		var parent = active_chat_widget.get_selected().get_parent()
		if parent:
			var account = parent.get_metadata(0)
			account_name = account.account_name
		chat_view.roster_item = metadata
		var chat_thread = metadata.chat_thread
		chat_thread.account = account_name
		chat_view.chat_thread = chat_thread

func start_edit_roster_info(roster_item:XMPPRosterItem):
	edit_roster_info.jid = roster_item.jid
	edit_roster_info.friendly_name = roster_item.friendly_name
	edit_roster_info.groups = roster_item.groups
	edit_roster_info.connect("confirmed", self, "finish_edit_roster_info", [roster_item], CONNECT_ONESHOT)
	edit_roster_info.popup_centered()

func finish_edit_roster_info(roster_item:XMPPRosterItem):
	# Check if anything changed.
	if edit_roster_info.friendly_name != roster_item.friendly_name or edit_roster_info.groups != roster_item.groups:
		roster_item.friendly_name = edit_roster_info.friendly_name
		roster_item.groups = edit_roster_info.groups
		roster_item.update_and_sent_query()
