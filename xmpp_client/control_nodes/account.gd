###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends Control

###
# The account is a control that represents a connected account.
# It holds the stream (and connects to it), a queryprocessor for queries,
# a roster for handling the contacts and chat messages, and a dropdown for
# the broadcasted presence.
#
# You can access the chat messages via the roster items in the roster.
# You can access the account's presence via the presence dropdown, and contacts
# presences are handled by the roster items in the roster.
###

var stream_features = XMPPStreamFeatures.new()

const InfoQueryProcessor = preload("res://xmpp_client/xmpp/xmpp_info_query_processor.gd")
var info_query_processor = InfoQueryProcessor.new()

const Roster = preload("res://xmpp_client/xmpp/Roster/xmpp_roster.gd")
var roster = Roster.new()

onready var dropdown_presence = get_node("VBoxContainer/PresenceDropdown")
onready var account_info = get_node("VBoxContainer/HBoxContainer/VBoxContainer/account_info")
onready var stream = get_node("VBoxContainer/HBoxContainer/VBoxContainer/Stream")

var account_name setget set_account_name, get_account_name

# Send out debug data.
signal debug(debug_data)
# Send out an update from the roster.
signal roster_update(acc_name)
# Send out a presence update from the roster.
signal roster_presence_update()
# Send out an error message.
signal error_message(error_message, error_stanza, account_name)
# Send out a message that the stream failed and the account should be removed.
signal stream_failed(account_name)

func _ready():

	dropdown_presence.connect("item_selected", self, "broadcast_presence")
	account_info.append_bbcode(" Account: % \n Server: %\n Port: %".format([stream.account_name, stream.server_ip, stream.port_number], "%"))

	info_query_processor.connect("new_iq_stanza", self, "send_data")
	info_query_processor.connect("debug", self, "pass_on_debug")
	info_query_processor.connect("error_message", self, "pass_on_error_stanza")

	roster.connect("send_out_iq", info_query_processor, "add_info_query")
	roster.connect("roster_updated", self, "pass_on_roster_update")
	roster.connect("roster_presence_update", self, "pass_on_roster_presence_update")
	roster.connect("send_out_stanza", self, "send_data")
	info_query_processor.connect("roster_push", roster, "receive_roster_push")

	add_child(roster, true)
	add_child(info_query_processor, true)

###
# Setup the stream and connect.
###
func setup_stream(ip :String, port :int, JID :String, accountPassword :String):
	stream.server_ip = ip
	stream.account_name = JID
	stream.port_number = port
	stream.password = accountPassword
	stream.connect("debug", self, "pass_on_debug")
	stream.connect("new_stanza", self, "receive_new_stanza")
	stream.connect("error_message", self, "pass_on_error_stream")
	stream.locale = TranslationServer.get_locale().replace("_", "-")
	stream.connect_stream()

###
# Disconnected stream.
###
func disconnect_account():
	stream.end_stream()

###
# Receive a new stanza and send it either to the query processor or the roster.
###
func receive_new_stanza(stanza: String):
	if stanza.begins_with("<stream:features"):
		stream_features.parse_from_xml(stanza)
		process_final_stream_features()
	elif stanza.begins_with("<iq"):
		info_query_processor.parse_info_query_type(stanza)
	elif stanza.begins_with("<presence"):
		roster.receive_presence(stanza, stream.account_name)
	elif stanza.begins_with("<message"):
		roster.receive_message(stanza)
	else:
		pass_on_debug("New unrecognisable stanza: "+stanza)

###
# After the stream is negotiated, there's still resource binding and possibly
# other features to be done. These should be processed here.
# After that, request roster and broadcast first presence.
###
func process_final_stream_features(feature_complete:String = ""):
	if !feature_complete.empty():
		stream_features.feature_done(feature_complete)
	prints("Features to handle:", stream_features.parsedDictionary)

	var required_handled = true
	for key in stream_features.parsedDictionary.keys():
		if stream_features.parsedDictionary[key] == XMPPStreamFeatures.FeatureReq.REQUIRED:
			required_handled = false
			if key == "bind":
				resource_bind()
	if required_handled:
		if stream_features.parsedDictionary.has("ver"):
			request_roster_get(true)
			stream_features.feature_done("ver")
		else:
			request_roster_get(false)
		broadcast_presence()
	pass

###
# Generate a resource bind.
###
func resource_bind():
	var bind_query = XMPPResourceBind.new()
	bind_query.connect("query_succesful", self, "process_final_stream_features", ["bind"], CONNECT_ONESHOT)
	info_query_processor.add_info_query(bind_query)

###
# Get the roster from server by generating a roster request.
# We're setting the roster on the request so we don't have to
# worry about receiving update signals.
###
func request_roster_get(versioned:bool):
	var info_query = XMPPRosterGet.new()
	info_query.version_get = versioned
	info_query.version = roster.version
	info_query.connect("response", roster, "parse_with_parser", [], CONNECT_ONESHOT)
	info_query_processor.add_info_query(info_query)

###
# Broadcast our current presence. This allows us to use the presence dropdown to
# go online, offline, and go online with the appropriate status.
###
func broadcast_presence(_val:int = 0):
	var presence = dropdown_presence.get_selected_metadata()
	send_presence(presence)
###
# Send presence data.
###
func send_presence(presence):
	if presence.type != XMPPPresence.Type.UNAVAILABLE and stream.stream_status == stream.StreamState.END:
		stream.connect_stream()

	if stream.stream_status != stream.StreamState.STANZA:
		return

	stream.send_string(presence.xml)

	if presence.type == XMPPPresence.Type.UNAVAILABLE:
		disconnect_account()

###
# Send xml string
###
func send_data(xml:String):
	stream.send_string(xml)

###
# Getters and setters for the account_name
###
func get_account_name():
	return stream.account_name

func set_account_name(val:String):
	stream.account_name = val

###
# Pass on debug to display in debug output.
###
func pass_on_debug(debug_data: String):
	emit_signal("debug", debug_data)

###
# Pass on stream error for display, and request the account to be removed.
###
func pass_on_error_stream(error_message:String, error_stanza:String):
	emit_signal("error_message", error_message, error_stanza, stream.account_name)
	emit_signal("stream_failed", stream.account_name)

###
# Pass on error stanza so it can be displayed.
###
func pass_on_error_stanza(error_message:String, error_stanza:String):
	emit_signal("error_message", error_message, error_stanza, stream.account_name)

###
# Pass on roster updates so the roster gui can be refilled.
###
func pass_on_roster_update():
	emit_signal("roster_update", get_account_name())
###
# Pass on presence updates so the roster gui can be redrawn.
###
func pass_on_roster_presence_update():
	emit_signal("roster_presence_update")
