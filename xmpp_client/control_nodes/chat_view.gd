###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends VBoxContainer

const ChatMessage = preload("res://xmpp_client/xmpp/Message/xmpp_chat_message.tscn")

onready var message_view = get_node("MessageView/VBoxContainer")
onready var message_view_scroll = get_node("MessageView").get_v_scrollbar()
onready var btn_send = get_node("PanelContainer/HBoxContainer/BtnSend")
onready var chat_input = get_node("PanelContainer/HBoxContainer/ChatInput")
onready var chat_question = get_node("ChatQuestion")

###
# GUI item that holds the chat thread and the related messages.
###

var roster_item setget set_roster_item
var chat_thread setget set_chat_thread

func _ready():
	btn_send.connect("pressed", self, "send_message")

###
# Setup the chat question subscription message.
###
func setup_subscription_message():
	if chat_question.is_visible_in_tree():
		print("not visble")
		return
	chat_question.subject_label.text = tr("Subscription Request")
	var extra_message = ""
	if !roster_item.presence.status.empty():
		extra_message = "They added the following message: %".format([roster_item.presence.status], "%")
	chat_question.rich_text.bbcode_text = tr("% wants to subscribe to you. %").format([roster_item.jid, extra_message], "%")

	var subscribe_btn = Button.new()
	subscribe_btn.text = tr("Accept")
	subscribe_btn.connect("pressed", roster_item, "accept_subscription")
	subscribe_btn.connect("pressed", chat_question, "hide_and_remove_buttons")
	chat_question.add_button(subscribe_btn)

	var unsubscribe_btn = Button.new()
	unsubscribe_btn.text = tr("Reject")
	unsubscribe_btn.connect("pressed", roster_item, "reject_subscription")
	unsubscribe_btn.connect("pressed", chat_question, "hide_and_remove_buttons")
	chat_question.add_button(unsubscribe_btn)
	chat_question.show()

###
# Set the roster item.
# This one doesn't work too great: if you get a subscription message during a chat,
# it doesn't show...
###
func set_roster_item(new_item: XMPPRosterItem):
	roster_item = new_item
	if roster_item.presence.type == XMPPPresence.Type.SUBSCRIBE:
		setup_subscription_message()

###
# Setter for the chat thread, empties the container and adds all messages manually.
###
func set_chat_thread(val:XMPPMessageThread):
	if chat_thread:
		chat_thread.disconnect("message_added", self, "add_message")
	chat_thread = val
	chat_thread.connect("message_added", self, "add_message")
	for childnode in message_view.get_children():
		message_view.remove_child(childnode)
	for message in chat_thread.messages:
		add_message(message)


###
# Add a message node to the thread. This is called by the chat_thread.
###
func add_message(message:XMPPMessage):
	var chat_message = ChatMessage.instance()
	chat_message.chatmessage = message
	message_view.add_child(chat_message)
	var message_yours = (message.sender != chat_thread.account and !message.sender.empty())
	chat_message.avatar_right = message_yours

	scroll_to_end()

###
# Small utility function that scrolls to the end if the scroll bar was already
# at the end before.
#
# This ensures we scroll down when necessary, but don't annoy the user when they
# are reading the backlog.
###
func scroll_to_end():
	var old_max_value = message_view_scroll.max_value
	yield(get_tree(), "idle_frame")
	if (message_view_scroll.page+message_view_scroll.value) == old_max_value:
		message_view_scroll.value = message_view_scroll.max_value

###
# Send a message from the chat input
###
func send_message():
	var message = XMPPMessage.new()

	message.message_body = chat_input.text
	message.type = XMPPMessage.Type.CHAT

	chat_input.text = ""
	if chat_thread:
		chat_thread.send_message(message)
