extends Resource

###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

###
# Chat thread keeps a list of related messages.
###

class_name XMPPMessageThread

var messages = []
var thread = ""
var parent_thread = ""
var message_address = ""
var account = ""

signal send_message(message)
signal message_added(message)

###
# Add a message and send out a signal for gui to update.
###
func add_message(message:XMPPMessage):
	messages.append(message)

	emit_signal("message_added", message)

###
# Send a message, this will be added and a signal will be send to have the
# message transported over the stream.
###
func send_message(message:XMPPMessage):
	message.recipient = message_address
	message.thread = thread
	add_message(message)
	emit_signal("send_message", message)
