###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends XMPPStanza

###
# Message
#
# An IM!
###

class_name XMPPMessage

enum Type{
	UNDEFINED,# XMPP core doesn't specify messages should have these, so hence
	CHAT,     # regular IM
	ERROR,    # An error
	GROUPCHAT,# This is like a groupchat.
	HEADLINE, # This is like an alert or server notice.
	NORMAL    # This one's like an email.
}

var type := 0
export(String) var message_subject = ""
export(String) var message_body = ""
export(String) var thread = ""
export(String) var parent_thread = ""
var time_stamp = {}

func _init():
	time_stamp = OS.get_datetime()

func generate_stanza(contents: String) -> String:

	var stanza = []

	stanza.append("<message")
	stanza.append("id='%'".format([id], "%"))
	stanza.append("to='%'".format([recipient], "%"))
	if !xml_lang.empty():
		stanza.append("xml:lang='%'".format([xml_lang.replace("_", "-")], "%"))
	if type != Type.UNDEFINED:
		stanza.append("type='%'".format([type_to_string(type)], "%"))

	if contents.empty():
		stanza.append("/>")
	else:
		stanza.append(">"+contents+"</message>")

	return PoolStringArray(stanza).join(" ")

func stanza() -> String:
	# Messages MUST have an addresse

	var contents = []

	if !message_subject.empty():
		contents.append("<subject>%</subject>".format([message_subject.xml_escape()], "%"))
	if !message_body.empty():
		contents.append("<body>%</body>".format([message_body.xml_escape()], "%"))
	if !thread.empty():
		contents.append("<thread")
		if !parent_thread.empty():
			contents.append("parent='%' >%</thread>".format([parent_thread, thread], "%"))
		else:
			contents.append(">%</thread>".format([thread], "%"))
	if recipient.empty():
		return ""
	else:
		return generate_stanza(PoolStringArray(contents).join(" "))

func set_from_xml(xml:String) -> int:
	var result = OK
	var parser = XMLParser.new()
	parser.open_buffer(xml.to_utf8())

	if parser.read() == OK:
		recipient = parser.get_named_attribute_value_safe("to")
		full_sender = parser.get_named_attribute_value_safe("from")
		sender = full_sender.split("/")[0]
		type = string_to_type(parser.get_named_attribute_value_safe("type"))
		xml_lang = parser.get_named_attribute_value_safe("xml:lang").replace("-", "_")
		if parser.has_attribute("id"):
			id = parser.get_named_attribute_value("id")
	if type == Type.ERROR:
		var error = XMPPStanzaError.new()
		result = error.parse_with_parser(parser)
		print(error.error_type)
	elif !parser.is_empty():
		while parser.read() == OK:
			if parser.get_node_type() == XMLParser.NODE_ELEMENT:
				if parser.get_node_name() == "subject":
					result = parser.read()
					message_subject = parser.get_node_data()
				elif parser.get_node_name() == "body":
					result = parser.read()
					message_body = parser.get_node_data()
				elif parser.get_node_name() == "thread":
					if parser.has_attribute("parent"):
						parent_thread = parser.get_named_attribute_value("parent")
					result = parser.read()
					thread = parser.get_node_data()
	else:
		result = ERR_INVALID_DATA
	return result

###
# Convenience functions to map the xml data to the enum.
###

func type_to_string(value :int) -> String:
	var type_name = ""
	if value == Type.UNDEFINED:
		return type_name
	for i in Type.keys():
		if Type[i] == value:
			type_name = str(i).to_lower()
	return type_name

func string_to_type(text:String) -> int:
	if text.empty():
		return Type.UNDEFINED
	else:
		return Type[text.to_upper()]
