###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends PanelContainer

var chatmessage = XMPPMessage.new()

###
# GUI element for showing a message.
###

onready var body = get_node("Main/MessageContainer/Body")
onready var header = get_node("Main/MessageContainer/Header")
onready var avatar = get_node("Main/AvatarContainer/Avatar")
onready var time_stamp = get_node("Main/AvatarContainer/TimeStamp")

onready var avatar_container = get_node("Main/AvatarContainer")
onready var message_container = get_node("Main/MessageContainer")
onready var main_layout = get_node("Main")

export(bool) var avatar_right = false setget set_avatar_container_right

func _ready():
	body.clear()
	body.text = chatmessage.message_body
	header.clear()
	if !chatmessage.message_subject.empty():
		header.text = chatmessage.message_subject
	else:
		if chatmessage.sender.empty():
			header.append_bbcode(tr("You say:"))
		else:
			header.append_bbcode(tr("% says:".format([chatmessage.sender], "%")))
	if chatmessage.time_stamp:
		time_stamp.text = PoolStringArray(chatmessage.time_stamp.values()).join("-")
	pass

###
# Set the avatar container to the right instead of the left.
###
func set_avatar_container_right(val:bool):
	avatar_right = val
	if val:
		main_layout.remove_child(avatar_container)
		main_layout.add_child(avatar_container)
	else:
		main_layout.remove_child(message_container)
		main_layout.add_child(message_container)

