###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends Node

###
# iq stanzas (info/queries) are a bit tricky. We could receive any stanza at any
# given moment, and we need to wait for results too.
#
# This class holds a list of ongoing queries. during process, it checks
# all of them, and how much they've progressed.
###
var info_query_list = {}

signal new_iq_stanza(iq)
signal roster_push(version, roster_item)
signal error_message(error_message, error_stanza)
signal debug(debug_data)

func _ready():
	pass

func _process(_delta):

	for key in info_query_list.keys():
		var query = info_query_list[key]
		var query_state = query.process()

		if query_state == XMPPInfoQuery.InfoQueryState.SEND_DATA:
			emit_signal("new_iq_stanza", query.xml)
			query.state = XMPPInfoQuery.InfoQueryState.WAITING
		elif query_state == XMPPInfoQuery.InfoQueryState.DONE:
			info_query_list.erase(query.id)
			emit_signal("debug", "finished query")
		else:
			# maybe put in a
			pass

func add_info_query(query:XMPPInfoQuery):
	info_query_list[query.id] = query

###
# Check if the incoming InfoQuery is a response, if so let that InfoQuery process that.
# otherwise, create a new InfoQuery resource.
###
func parse_info_query_type(xml:String):

	var parser = XMLParser.new()

	var id = "0"
	var type = "error"

	parser.open_buffer(xml.to_utf8())
	if parser.read() == OK:
		type = parser.get_named_attribute_value("type")
		id = parser.get_named_attribute_value("id")

	if type == "result" or type == "error":
		var query = info_query_list[id]
		if type == "error":
			var stanzas = tr("Error stanza:\n\n%\n\nOriginal stanza:\n\n%".format([xml, query.xml], "%"))
			query.connect("error_message", self, "pass_on_error", [stanzas], CONNECT_ONESHOT)
		query.parse_result_xml(xml)
	else:
		info_query_from_incoming_xml(id, type, parser)

func pass_on_error(error_message:String, error_stanza:String):
	emit_signal("error_message", error_message, error_stanza)

###
# Create a new InfoQuery from incoming xml.
# By default this creates a feature-not-implemented sult InfoQuery.
###
func info_query_from_incoming_xml(id:String, _type:String, parser:XMLParser):
	var query = XMPPInfoQuery.new()

	query.id = id
	var recipient = parser.get_named_attribute_value_safe("from").split("/")[0]
	if parser.read() == OK:
		if parser.get_named_attribute_value_safe("xmlns") == "jabber:iq:roster":
			query = XMPPRosterPush.new()
			query.parse_from_parser(parser)
			emit_signal("roster_push", query.version, query.roster_item)

	query.recipient = recipient

	add_info_query(query)
