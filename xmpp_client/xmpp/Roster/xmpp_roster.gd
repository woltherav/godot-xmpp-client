###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends Node

###
# The Roster is defined in XMPP-IM as a roster of contacts.
#
# Todo: presence-sub-preapproval(?)
###

export(String) var xml setget set_from_xml, get_xml_serialization

# Roster version
export(bool) var handle_version = false
export(String) var version = ""

# Roster items are the contacts.
var roster_items = {}

signal roster_updated()
signal roster_presence_update()
signal send_out_iq(query)
signal send_out_stanza(stanza)

func _ready():
	self.set_process(true)

func _process(_delta):
	for roster_item in roster_items.values():
		for stanza in roster_item.outbound_messages:
			if stanza.xml.begins_with("<iq"):
				emit_signal("send_out_iq", stanza)
			else:
				emit_signal("send_out_stanza", stanza.xml)
		roster_item.outbound_messages.clear()

###
# When the server's roster updates, it will sent out a
# roster push with a new version.
# We catch that in the InfoQuery processor, and receive it here.
###
func receive_roster_push(ver:String, update_item:XMPPRosterItem):
	version = ver
	if update_item:
		update_roster_item(update_item)
	else:
		print("strange...", update_item)

###
# Update a roster item without any signals.
# We need to copy the presence and the subscription id.
###
func update_roster_item(update_item:XMPPRosterItem):
	var item = get_roster_item(update_item.jid)
	if update_item.subscription_type == XMPPRosterItem.SubscriptionType.REMOVE:
		roster_items.erase(item)
	else:
		item.xml = update_item.xml
		roster_items[update_item.jid] = item
		emit_signal("roster_updated")

###
# Receive a presence change.
###
func receive_presence(xml_string:String, account_name:String):
	var presence = XMPPPresence.new()
	presence.xml = xml_string
	if presence.sender == account_name:
		return
	if presence.type == XMPPPresence.Type.UNDEFINED or presence.type == XMPPPresence.Type.UNAVAILABLE:
		get_roster_item(presence.sender).presence = presence
	elif (presence.type == XMPPPresence.Type.SUBSCRIBE):
		var roster_item = get_roster_item(presence.sender)
		roster_item.presence = presence
	if (presence.type == XMPPPresence.Type.UNSUBSCRIBED):
		get_roster_item(presence.sender).reject_subscription()
	emit_signal("roster_presence_update")

###
# receive a message and attach it to a roster item.
###
func receive_message(xml_string:String):
	var message = XMPPMessage.new()
	message.set_from_xml(xml_string)
	var roster_item = get_roster_item(message.sender)
	roster_item.receive_message(message)

###
# Get a given roster item, or make a new one.
# The new item does not trigger a roster_set as the user hasn't subscribed.
###
func get_roster_item(jid:String) -> XMPPRosterItem:
	if !roster_items.has(jid):
		var roster_item = XMPPRosterItem.new()
		roster_item.jid = jid
		roster_items[jid] = roster_item
		emit_signal("roster_updated")
	return roster_items[jid]

###
# Set from an xml string, sets up parser and calls the parser function.
###
func set_from_xml(xml_string:String):
	var parser = XMLParser.new()
	parser.open_buffer(xml_string.to_utf8())
	if parser.read() == OK:
		parse_with_parser(parser)


func parse_with_parser(parser: XMLParser):
	version = parser.get_named_attribute_value_safe("ver")
	if !parser.is_empty():
		if parser.read() == OK and parser.get_node_type() == XMLParser.NODE_ELEMENT:
			if parser.get_node_name() == "item":
				var item = XMPPRosterItem.new()
				item.parse_with_parser(parser)
				update_roster_item(item)

###
# Only really relevant for saving
###
func get_xml_serialization() -> String:
	var xml_string = ""
	xml_string += "<query xmlns='jabber:iq:roster'"
	if !version.empty():
		xml_string += " ver='%'".format([version], "%")
	if roster_items.empty():
		xml_string += "/>"
	else:
		xml_string += ">"
		for item in roster_items.keys():
			xml_string += roster_items[item].xml
		xml_string += "</query>"
	return xml_string

func _to_string() -> String:
	return get_xml_serialization()
