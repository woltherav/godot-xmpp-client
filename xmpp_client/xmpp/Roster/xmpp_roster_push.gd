###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends XMPPInfoQuery

class_name XMPPRosterPush
###
# A pushed update from the server that a given roster item changed.
###

var version := ""
var roster_item = XMPPRosterItem.new()

func _init():
	type = Type.RESULT
	pass

func stanza():
	return .generate_stanza(xml)

func parse_from_parser(parser:XMLParser):
	if parser.get_node_name() == "query" and parser.get_named_attribute_value("xmlns") == "jabber:iq:roster":
		version = parser.get_named_attribute_value_safe("ver")
	while parser.read() == OK:
		if parser.get_node_type() == XMLParser.NODE_ELEMENT:
			if parser.get_node_name() == "item" and parser.has_attribute("jid"):
				roster_item = XMPPRosterItem.new()
				roster_item.parse_with_parser(parser)
