###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends XMPPInfoQuery

class_name XMPPRosterSet

###
# Sents out a request for setting a roster-item, and then listens for said roster.
###

var roster_item_stanza := ""

func _init():
	type = Type.SET
	pass

func stanza():
	var xml = ""
	xml += "<query xmlns='jabber:iq:roster'>%</query>".format([roster_item_stanza], "%")
	return .generate_stanza(xml)

func parse_result(_parser:XMLParser):
	state = InfoQueryState.DONE
