###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends XMPPInfoQuery

class_name XMPPRosterGet
###
# Sents out a request for getting the roster, and then listens for said roster.
###

var version := ""
# Older servers don't support roster versioning.
var version_get = false

signal response(parser)

func _init():
	type = Type.GET
	pass

func stanza():
	var xml = ""
	var version_string = ""
	if version_get:
		version_string = "ver='%'".format([version], "%")
	xml += "<query xmlns='jabber:iq:roster' %/>".format([version_string], "%")
	return .generate_stanza(xml)

func parse_result(parser:XMLParser):
	while parser.read() == OK:
		if parser.get_node_type() == XMLParser.NODE_ELEMENT:
			if parser.get_node_name() == "query" and parser.get_named_attribute_value("xmlns") == "jabber:iq:roster":
				emit_signal("response", parser)
				state = InfoQueryState.DONE
