###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###
extends Resource

class_name XMPPRosterItem

###
# RosterItem represents a contact.
# It also keeps the related chat-thread with this contact as well
# as the last sent presence.
#
# Also handles sending messages to that contact (listening to the chat thread)
# sending out subscriptions and similar actions, and finally queries to
# handle it's information being updated.
###

enum SubscriptionType {
	NONE,
	TO,
	FROM,
	BOTH,
	REMOVE
}

export(String) var xml setget set_from_xml, get_xml_serialization

# A Jabber ID
export(String) var jid = ""
export(String) var friendly_name = ""

export(SubscriptionType) var subscription_type = SubscriptionType.NONE
export(PoolStringArray) var groups = []

export(String) var asks_subscription = ""

# current presence
var presence = XMPPPresence.new()
# related chat-thread
var chat_thread = XMPPMessageThread.new()

export(Texture) var avatar = load("res://icon.png")

# outbound messages
var outbound_messages = []

func _init():
	chat_thread.connect("send_message", self, "pass_on_message")

# Only relevant if the server supports preapproved...
export(bool) var preapproved = false

###
# Send out subscription request to the roster item.
###
func send_subscription_request(subscription_message:String):
	var subscribe = XMPPPresence.new()
	subscribe.recipient = jid
	subscribe.status = subscription_message
	subscribe.type = XMPPPresence.Type.SUBSCRIBE
	outbound_messages.append(subscribe)

###
# Subscribed is to send out an approval of a subscribtion request.
#
# According to XMPP-IM, it's recommended to send out the approval with the same
# jid as the presence that request it.
###
func accept_subscription():
	var subscribed = XMPPPresence.new()
	if presence.type == XMPPPresence.Type.SUBSCRIBE:
		subscribed.id = presence.id
	subscribed.recipient = jid
	subscribed.type = XMPPPresence.Type.SUBSCRIBED
	outbound_messages.append(subscribed)
	send_subscription_request("")
###
# Send ubsubscription request.
###
func send_unsubscribe():
	var unsubscribe = XMPPPresence.new()
	unsubscribe.recipient = jid
	unsubscribe.type = XMPPPresence.Type.UNSUBSCRIBE
	outbound_messages.append(unsubscribe)

###
# Unsubscribed is to send out an dismissal of a subscribtion request, even
# when previously approved.
#
# According to XMPP-IM, it's recommended to send out the dismissal with the same
# jid as the presence that request it.
###
func reject_subscription():
	var unsubscribed = XMPPPresence.new()
	unsubscribed.recipient = jid
	unsubscribed.type = XMPPPresence.Type.UNSUBSCRIBED
	outbound_messages.append(unsubscribed)

###
# Receive an incoming message.
###
func receive_message(message:XMPPMessage):
	chat_thread.message_address = jid
	chat_thread.add_message(message)

###
# Pass on message from chat thread to the outbound messaes.
###
func pass_on_message(message:XMPPMessage):
	message.recipient = jid
	outbound_messages.append(message)

###
# Generate set query, should be sent only if the query is genuinely changed.
###

func update_and_sent_query():
	var roster_set_query = XMPPRosterSet.new()
	roster_set_query.roster_item_stanza = get_xml_serialization()
	outbound_messages.append(roster_set_query)

###
# Set from xml string, will not change the presence or chat thread
###
func set_from_xml(xml_string:String):
	var parser = XMLParser.new()
	parser.open_buffer(xml_string.to_utf8())
	if parser.read() == OK:
		parse_with_parser(parser)
###
# Actually parse the xml.
###
func parse_with_parser(parser: XMLParser):
	jid = parser.get_named_attribute_value_safe("jid")
	friendly_name = parser.get_named_attribute_value_safe("name")
	subscription_type = string_to_subscription_type(parser.get_named_attribute_value_safe("subscription"))
	asks_subscription = parser.get_named_attribute_value_safe("ask")

	groups = []
	# need to change this to a while loop...
	if !parser.is_empty():
		if parser.read() == OK and parser.get_node_type() == XMLParser.NODE_ELEMENT:
			if parser.get_node_name() == "group":
				parse_group(parser)

###
# Parse the group tags
###
func parse_group(parser: XMLParser):
	while parser.read() == OK:
		if parser.get_node_type() == XMLParser.NODE_TEXT:
			groups.append(parser.get_node_data())
		elif parser.get_node_type() == XMLParser.NODE_ELEMENT_END:
			if parser.get_node_name() != "group":
				return
###
# Create xml version for rosterset and storage.
###
func get_xml_serialization() -> String:
	var xml_string = ""
	var gather_attributes = []

	gather_attributes.append("jid='%'".format([jid], "%"))
	if !friendly_name.empty():
		gather_attributes.append("name='%'".format([friendly_name], "%"))
	gather_attributes.append("subscription='%'".format([subscription_type_to_string(subscription_type)], "%"))

	if !asks_subscription.empty():
		gather_attributes.append("ask='%'".format([asks_subscription], "%"))

	xml_string += "<item "
	xml_string += PoolStringArray(gather_attributes).join(" ")

	if groups.empty():
		xml_string +="/>"
	else:
		xml_string += ">"
		for group in groups:
			xml_string += "<group>%</group>".format([group], "%")
		xml_string += "</item>"
	return xml_string

func _to_string():
	return get_xml_serialization()

###
# Convenience functions to map the xml data to the enum.
###
func subscription_type_to_string(value :int) -> String:
	var type_name = ""
	for i in SubscriptionType.keys():
		if SubscriptionType[i] == value:
			type_name = str(i).to_lower()
	return type_name

func string_to_subscription_type(text:String) -> int:
	if text.empty():
		return SubscriptionType.ERROR
	else:
		return SubscriptionType[text.to_upper()]
