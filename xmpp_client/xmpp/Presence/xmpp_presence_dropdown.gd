###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends OptionButton


###
# A little option dropdown that handles selecting and sending presence.
###

func _init():
	var p_chat = XMPPPresence.new()
	p_chat.show = XMPPPresence.ShowPresence.CHAT
	p_chat.status = tr("Online")
	add_presence_type(p_chat)

	var p_away = XMPPPresence.new()
	p_away.show = XMPPPresence.ShowPresence.AWAY
	p_away.status = tr("Away")
	add_presence_type(p_away)

	var p_dnd = XMPPPresence.new()
	p_dnd.show = XMPPPresence.ShowPresence.DND
	p_dnd.status = tr("Do not disturb")
	add_presence_type(p_dnd)

	var p_xa = XMPPPresence.new()
	p_xa.show = XMPPPresence.ShowPresence.XA
	p_xa.status = tr("Unknown when back")
	add_presence_type(p_xa)

	var p_offline = XMPPPresence.new()
	p_offline.type = XMPPPresence.Type.UNAVAILABLE
	p_offline.status = tr("Offline")
	add_presence_type(p_offline)


func add_presence_type(presence: XMPPPresence):
	var idx = get_item_count()
	add_icon_item(presence.get_status_icon(), presence.status, idx)
	set_item_metadata(idx, presence)
