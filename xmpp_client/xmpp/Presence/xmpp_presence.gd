###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends XMPPStanza

###
# Presence
#
# A presence is basically a status update, but can also be used to handle
# subscription requests.
###

class_name XMPPPresence

enum Type {
	UNDEFINED,   # XMPP-CORE doesn't define types, so we need a way to talk about type-less presences.
	ERROR,       # An error!
	PROBE,       # Check for a status update... Should only be the problem of the server
	SUBSCRIBE,   # Request a subscription
	SUBSCRIBED,  # Accept a subscription
	UNAVAILABLE, # User offline
	UNSUBSCRIBE, # Cancel a subscription.
	UNSUBSCRIBED # Rejects a subscription.
}
var type = Type.UNDEFINED

enum ShowPresence{
	UNDEFINED,
	CHAT,
	AWAY,
	DND,
	XA
}
var show = ShowPresence.UNDEFINED
var status = ""
var priority = 0

var icon_chat = load("res://xmpp_client/icons/presence_chat.svg")
var icon_away = load("res://xmpp_client/icons/presence_away.svg")
var icon_subscribe = load("res://xmpp_client/icons/presence_subscribe.svg")
var icon_extended_away = load("res://xmpp_client/icons/presence_extended_away.svg")
var icon_busy = load("res://xmpp_client/icons/presence_busy.svg")

func _ready():
	pass

func generate_stanza(contents: String) -> String:

	var stanza = []

	stanza.append("<presence")
	stanza.append("id='%'".format([id], "%"))
	if !recipient.empty():
		stanza.append("to='%'".format([recipient], "%"))
	if !xml_lang.empty():
		stanza.append("xml:lang='%'".format([xml_lang.replace("_", "-")], "%"))
	if type != Type.UNDEFINED:
		stanza.append("type='%'".format([type_to_string(type)], "%"))

	if contents.empty():
		stanza.append("/>")
	else:
		stanza.append(">"+contents+"</presence>")

	return PoolStringArray(stanza).join(" ")

func stanza() -> String:

	var contents = []

	var show_string = showpresence_to_string(show)
	if !show_string.empty():
		contents.append("<show>%</show>".format([show_string], "%"))
	if !status.empty():
		contents.append("<status>%</status>".format([status.xml_escape()],"%"))
	if priority != 0:
		contents.append("<priority>%</priority>".format([priority],"%"))

	return generate_stanza(PoolStringArray(contents).join(""))

func set_from_xml(xml:String) -> int:
	var result = OK
	var parser = XMLParser.new()
	parser.open_buffer(xml.to_utf8())

	if parser.read() == OK:
		recipient = parser.get_named_attribute_value_safe("to")
		full_sender = parser.get_named_attribute_value_safe("from")
		sender = full_sender.split("/")[0]
		type = string_to_type(parser.get_named_attribute_value_safe("type"))
		xml_lang = parser.get_named_attribute_value_safe("xml:lang").replace("-", "_")
		if parser.has_attribute("id"):
			id = parser.get_named_attribute_value("id")
	if type == Type.ERROR:
		var error = XMPPStanzaError.new()
		result = error.parse_with_parser(parser)
		print(error.error_type)
	elif !parser.is_empty():
		while parser.read() == OK:
			if parser.get_node_type() == XMLParser.NODE_ELEMENT:
				if parser.get_node_name() == "show":
					result = parser.read()
					show = string_to_showpresence(parser.get_node_data())
				elif parser.get_node_name() == "status":
					result = parser.read()
					status = parser.get_node_data()
				elif parser.get_node_name() == "priority":
					result = parser.read()
					priority = int(parser.get_node_data())
				else:
					parser.skip_section()
	return result

func get_status_icon() -> Texture:
	var icon = ImageTexture.new()
	if type == Type.SUBSCRIBE:
		icon = icon_subscribe
	elif type != Type.UNAVAILABLE:
		if show == ShowPresence.CHAT:
			icon = icon_chat
		elif show == ShowPresence.AWAY:
			icon = icon_away
		elif show == ShowPresence.DND:
			icon = icon_busy
		elif show == ShowPresence.XA:
			icon = icon_extended_away
	return icon

###
# Convenience functions to map the xml data to the enums.
###

func type_to_string(value :int) -> String:
	var type_name = ""
	if value == Type.UNDEFINED:
		return type_name
	for i in Type.keys():
		if Type[i] == value:
			type_name = str(i).to_lower()
	return type_name

func string_to_type(text:String) -> int:
	if text.empty():
		return Type.UNDEFINED
	else:
		return Type[text.to_upper()]

func showpresence_to_string(value :int) -> String:
	var type_name = ""
	if value == ShowPresence.UNDEFINED:
		return type_name
	for i in ShowPresence.keys():
		if ShowPresence[i] == value:
			type_name = str(i).to_lower()
	return type_name

func string_to_showpresence(text:String) -> int:
	if text.empty():
		return ShowPresence.UNDEFINED
	else:
		return ShowPresence[text.to_upper()]
