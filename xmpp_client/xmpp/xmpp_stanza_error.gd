###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends Resource

class_name XMPPStanzaError

###
# Class for parsing stanza errors.
#
# TODO:
# Handle XEP 0086
###

enum StanzaErrorTypes{
	AUTH,    # Requires authorization.
	CANCEL,  # Must be cancelled
	CONTINUE,# Just a warning, we can continue
	MODIFY,  # Modify the request and try again.
	WAIT     # Wait a bit.
}

enum StanzaErrorConditions {
	BAD_REQUEST,
	CONFLICT,
	FEATURE_NOT_IMPLEMENTED,
	FORBIDDEN,
	GONE,
	INTERNAL_SERVER_ERROR,
	ITEM_NOT_FOUND,
	JID_MALFORMED,
	NOT_ACCEPTABLE,
	NOT_ALLOWED,
	NOT_AUTHORIZED,
	POLICY_VIOLATION,
	RECIPIENT_UNAVAILABLE,
	REDIRECT,
	REGISTRATION_REQUIRED,
	REMOTE_SERVER_NOT_FOUND,
	REMOTE_SERVER_TIMEOUT,
	RESOURCE_CONSTRAINT,
	SERVICE_UNAVAILABLE,
	SUBSCRIPTION_REQUIRED,
	UNDEFINED_CONDITION,
	UNEXPECTED_REQUEST
}

var humane_error_messages = {
	StanzaErrorConditions.BAD_REQUEST:
		tr("The sent message cannot be processed at all."),
	StanzaErrorConditions.CONFLICT:
		tr("This resource already exists."),
	StanzaErrorConditions.FEATURE_NOT_IMPLEMENTED:
		tr("This feature is not implemented."),
	StanzaErrorConditions.FORBIDDEN:
		tr("You do not have the permission to perform this action, login required."),
	StanzaErrorConditions.GONE:
		tr("Server can no longer be contacted at this address."),
	StanzaErrorConditions.INTERNAL_SERVER_ERROR:
		tr("The server is experiencing issues, try again later."),
	StanzaErrorConditions.ITEM_NOT_FOUND:
		tr("The requested item or JID cannot be found."),
	StanzaErrorConditions.JID_MALFORMED:
		tr("The specified JID does is incorrectly formed."),
	StanzaErrorConditions.NOT_ACCEPTABLE:
		tr("The message cannot be accepted because it lacks necessary parameters."),
	StanzaErrorConditions.NOT_ALLOWED:
		tr("The client is not allowed to perform this action."),
	StanzaErrorConditions.NOT_AUTHORIZED:
		tr("The client is not allowed to perform the action because it is not authorized to do so."),
	StanzaErrorConditions.POLICY_VIOLATION:
		tr("The server determined the message violated terms of service in some manner."),
	StanzaErrorConditions.RECIPIENT_UNAVAILABLE:
		tr("The recipient is not available right now, trying again later."),
	StanzaErrorConditions.REDIRECT:
		tr("The server says this message should be redirected."),
	StanzaErrorConditions.REGISTRATION_REQUIRED:
		tr("Registration is required before this action can be performed."),
	StanzaErrorConditions.REMOTE_SERVER_NOT_FOUND:
		tr("The server of the recipient could not be found."),
	StanzaErrorConditions.REMOTE_SERVER_TIMEOUT:
		tr("The server of the recipient was found, but connection could not be established"),
	StanzaErrorConditions.RESOURCE_CONSTRAINT:
		tr("The server is too busy to handle this stream."),
	StanzaErrorConditions.SERVICE_UNAVAILABLE:
		tr("The requested service cannot be provided."),
	StanzaErrorConditions.SUBSCRIPTION_REQUIRED:
		tr("Subscription is required before you can perform this action"),
	StanzaErrorConditions.UNDEFINED_CONDITION:
		tr("A special case error has occured. See included xml stream."),
	StanzaErrorConditions.UNEXPECTED_REQUEST:
		tr("The message sent was understood, but not expected by the server.")
}

var error_type = StanzaErrorTypes.CANCEL
var error_condition = StanzaErrorConditions.SERVICE_UNAVAILABLE
var extra_data = ""

func _ready():
	pass

func parse_from_xml(xml:String):
	var parser = XMLParser.new()
	parser.open_buffer(xml.to_utf8())
	parse_with_parser(parser)

func parse_with_parser(parser:XMLParser):

	var found_error_message = false
	while parser.read() and parser.get_node_type() == XMLParser.NODE_ELEMENT and !found_error_message:
		if parser.get_node_name() == "error":
			found_error_message = true
		else:
			parser.skip_section()

	# Read the error element
	if parser.get_node_name() == "error":
		var error_name = parser.get_named_attribute_value_safe("type").to_upper()
		error_type = StanzaErrorTypes[error_name]

		if parser.is_empty():
			error_condition = StanzaErrorConditions.UNDEFINED_CONDITION
			extra_data = "Malformed error message!"
		else:
		# Read the error inside...
			if parser.read() == OK:
				error_name = parser.get_node_name().replace("-", "_").to_upper()
				error_condition = StanzaErrorConditions[error_name]
				if !parser.is_empty() and parser.read() == OK:
					extra_data = parser.get_node_data()
	return OK

func stanza() -> String:
	var stanza = []

	stanza.append("<error error-type='%'".format([error_type_for_enum(error_type)], "%"))

	if error_name_for_enum(error_condition).empty():
		stanza.append("/>")
	else:
		stanza.append("><"+error_name_for_enum(error_condition))
		stanza.append("xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'")
		if extra_data.empty():
			stanza.append("/>")
		else:
			stanza.append(">")
			stanza.append(extra_data)
			stanza.append("</"+error_condition+">")
		stanza.append("</error>")


	return PoolStringArray(stanza).join(" ")

func error_type_for_enum(value :int):
	var error_name = ""
	for i in StanzaErrorTypes.keys():
		if StanzaErrorTypes[i] == value:
			error_name = str(i).to_lower()
	return error_name

func error_name_for_enum(value :int):
	var error_name = ""
	for i in StanzaErrorConditions.keys():
		if StanzaErrorConditions[i] == value:
			error_name = str(i).replace("_", "-").to_lower()
	return error_name

func human_friendly_error_message():
	return humane_error_messages[error_condition]
