###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends XMPPInfoQuery

class_name XMPPResourceBind

export var device_name = ""

###
# Tell the server to which device to associate the client with.
#
# TODO
# accept error, modify device name, then try again.
###

func _init():
	self.type = Type.SET
	var project_name = str(ProjectSettings.get_setting("application/config/name"))
	device_name = project_name.replace(" ", "_").to_lower()

func stanza() -> String:
	var stanza = []

	stanza.append("<bind")
	stanza.append("xmlns='urn:ietf:params:xml:ns:xmpp-bind'")
	stanza.append(">")
	stanza.append("<resource>%</resource>".format([device_name], "%"))
	stanza.append("</bind>")

	return .generate_stanza(PoolStringArray(stanza).join(" "))
