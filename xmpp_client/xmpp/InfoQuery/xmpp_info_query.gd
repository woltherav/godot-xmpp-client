###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###

extends XMPPStanza

class_name XMPPInfoQuery

enum Type {
	GET,
	SET,
	RESULT,
	ERROR
	}
export var type = Type.ERROR

enum InfoQueryState {
	SEND_DATA,
	WAITING,
	DONE
}
var state = InfoQueryState.SEND_DATA

signal query_succesful()
signal error_message(error_message)

###
# InfoQuery, or the <iq /> stanza.
#
# These are messages that always have a response, compared to <presence /> and
# <message /> which do not. To handle the fact Queries need to wait around a
# lot, their processing is always handled by the InfoQueryProcessor.
#
# Queries start their life as 'send-data' queries, which the processor will
# use to send them over the stream. Once the processor has done that, the InfoQuery
# will be set to 'waiting'.
#
# Once a InfoQuery receives a result, it can handle that, and then either switch
# back to 'send-data' (for responses to errors) or set themselves as 'done'.
# queries that are done will be removed from the processor (and as resources,
# free themselves once there's no references to them anymore).
###

func process() -> int:

	return state

func generate_stanza(contents: String) -> String:
	var stanza = []

	stanza.append("<iq")
	stanza.append("id='%'".format([id], "%"))
	if !recipient.empty():
		stanza.append("to='%'".format([recipient], "%"))
	stanza.append("type='%'".format([type_to_string(type)], "%"))
	if contents.empty():
		stanza.append("/>")
	else:
		stanza.append(">"+contents+"</iq>")
	return PoolStringArray(stanza).join(" ")

func stanza() -> String:
	# empty queries should be interpreted as unimplemented features.

	var error = XMPPStanzaError.new()
	error.error_condition = XMPPStanzaError.StanzaErrorConditions.SERVICE_UNAVAILABLE
	return generate_stanza(error.stanza())

###
# Generic implementation of result.
# Default behaviour should be to interpret Result as 'done', so the resource
# can be removed. A specialized iq that 'get's data, should proly want to
# process it first.
#
# Errors should just mark the InfoQuery as done, maybe outputting the data to
# the debug or an error message.
# Others than these are an error in themselves...
#
###
func parse_result_xml(xml: String):
	var parser = XMLParser.new()
	parser.open_buffer(xml.to_utf8())
	if parser.read() == OK:
		var incoming_type = string_to_type(parser.get_named_attribute_value("type"))

		if incoming_type == Type.RESULT:
			parse_result(parser)

		elif incoming_type == Type.ERROR:
			parse_error(parser)

		else:
			emit_signal("error_message", "Error: incoming xml is of the same id as iq sent out")
			# should send out a unexpected request error..???

###
# Specialized parse result, in the subclassed infoqueries, this one should be
# reimplemented.
###
func parse_result(_parser:XMLParser):
	emit_signal("query_succesful")
	state = InfoQueryState.DONE

###
# Error handler. This should work for most infoqueries.
# Todo: By default this works for cancel and maybe continue.
# Auth, Modify and Wait still need to be handled.
###
func parse_error(parser:XMLParser):
	var error = XMPPStanzaError.new()
	error.parse_with_parser(parser)
	var error_name = error.error_name_for_enum(error.error_condition)
	var error_message = 'An iq stanza with error "%" was returned: %'.format([error_name, error.human_friendly_error_message()], "%")
	emit_signal("error_message", error_message)
	state = InfoQueryState.DONE

func set_from_xml(_xml:String) -> int:
	return ERR_CANT_CREATE

###
# Convenience functions to map the xml data to the enum.
###

func type_to_string(value :int) -> String:
	var type_name = ""
	for i in Type.keys():
		if Type[i] == value:
			type_name = str(i).to_lower()
	return type_name

func string_to_type(text:String) -> int:
	if text.empty():
		return Type.ERROR
	else:
		return Type[text.to_upper()]
