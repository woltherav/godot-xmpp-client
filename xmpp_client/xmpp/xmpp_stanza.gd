###
# This file is part of Godot XMPP Client
# SPDX-FileCopyrightText: 2020 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
#
# SPDX-License-Identifier: MIT
###
extends Resource

class_name XMPPStanza

export (String) var xml setget set_from_xml, stanza

export (String) var id
export (String) var recipient
export (String) var sender
export (String) var xml_lang

# Only for storing full jids, we need bare jids most of the time.
export (String) var full_sender

###
# Generic Stanza resource, to hold the information inside the incoming
# stanzas. Should be subclassed into InfoQuery, Message or Presence.
###

func _init():
	id = str(get_instance_id())

func stanza() -> String:
	return ""

func set_from_xml(_xml:String) -> int:
	return ERR_CANT_CREATE

###
# This gives the stanza in the debug output.
###
func _to_string():
	return stanza()
