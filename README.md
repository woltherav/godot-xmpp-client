Godot XMPP Client
=================

This is a small xmpp client written in Godot.

I made this to learn a bit more about both Godot and internet technologies. *I have no idea what I am doing, please send help.*

## Implementation state 

- XMPP-IM is completely implemented, with the exception of subscription pre-approval, mostly as I didn't understand the spec here.
- XMPP-Core is incomplete, because Godot... misses some core features:
	+ DNS SRV is missing in Godot.
	+ Godot has no options to do HMAC and thus we cannot write a SCRAM authentication type for SASL. Only supports SASL Plain over TLS right now.
	+ SSL seems to work, but I have admittedly no idea what I am doing...
	+ Stream reconnect is something I am also not sure on how to handle.
- Zero XEPs are implemented. I did read through several key ones when designing the architecture, but did not implement any of them to keep focus.

## Known bugs and limitations

- For some reason when you add two accounts, it freezes. I am unsure why this happens, but suspect it is caused when multiple streams connect to the same server and port, and one of those streams receives data. I think Godot is unable to tell the difference between the two? But I am unsure, because I made my first tcp connection a week ago as of writing...
- There's no saving of anything right now, so no account storing, no message saving, etc.
- The avatars don't do anything, these will require additional XEPs to be implemented.
- I have no idea how to generate random strings of alphanumerical characters, so everything that needs IDs uses the object id right now.
- No button to start a conversation with anyone not known to your account.

## License

Currently licensed under MIT, might change in the future if the more complex XEPs get implemented, as these may need GPL c++ libraries.

All icons besides icon.png are made by me and can be considered cc-by-4.0

## Other

- I really want to have unit testing before heading into XEP territory, as different XEPs change the layout of stanzas significantly. I would like to ensure every type of stanza can be parsed as appropriate.
- This was only tested against Prosody and only with test-accounts.